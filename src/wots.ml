open Utils

module WOTS : S.T = struct
  let message_length_in_bytes = 2

  let w_param = message_length_in_bytes * 8

  let w_param_int = Z.(to_int (pow Z.(one + one) w_param))

  type sk = Bytes.t

  let sk_size_in_bytes = 32

  type pk = Bytes.t

  let pk_size_in_bytes = 32

  type signature = Bytes.t

  let signature_size_in_bytes = 32

  let rec repeat_hash inp n =
    assert (n >= 0) ;
    if n = 0 then inp else repeat_hash (sha256 inp) (n - 1)

  let generate_keys () =
    let sk = generate_256bits () in
    let pk = repeat_hash sk w_param_int in
    (sk, pk)

  let sign msg sk =
    assert (Bytes.length msg = message_length_in_bytes) ;
    let n = bytes_to_int msg message_length_in_bytes in
    repeat_hash sk n

  let verify msg pk signature =
    assert (Bytes.length msg = message_length_in_bytes) ;
    let n = bytes_to_int msg message_length_in_bytes in
    Bytes.equal (repeat_hash signature (w_param_int - n)) pk
end

include WOTS

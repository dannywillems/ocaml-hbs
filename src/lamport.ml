open Utils

module Lamport : S.T = struct
  let message_length_in_bytes = 16

  let hash_size_in_bytes = 32

  (* We suppose a component is 256 bits long *)
  type sk = (Bytes.t * Bytes.t) array

  let sk_size_in_bytes = message_length_in_bytes * 8 * hash_size_in_bytes * 2

  (* We suppose a component is 256 bits long *)
  type pk = (Bytes.t * Bytes.t) array

  let pk_size_in_bytes = message_length_in_bytes * 8 * hash_size_in_bytes * 2

  type signature = Bytes.t array

  let signature_size_in_bytes = message_length_in_bytes * 8 * hash_size_in_bytes

  let generate_keys () =
    let sk =
      Array.init (message_length_in_bytes * 8) (fun _ ->
          (generate_256bits (), generate_256bits ()))
    in
    let pk = Array.map (fun (sk1, sk2) -> (sha256 sk1, sha256 sk2)) sk in
    (sk, pk)

  let sign msg sk =
    assert (Array.length sk = message_length_in_bytes * 8) ;
    assert (Bytes.length msg = message_length_in_bytes) ;
    let bits = Bit.le_bits_of_bytes_exn msg in
    let signature =
      Bit.mapi
        (fun i b ->
          let sk1, sk2 = Array.get sk i in
          if Bit.is_one b then sk2 else sk1)
        bits
    in
    Array.of_list signature

  let verify msg pk signature =
    assert (Array.length pk = message_length_in_bytes * 8) ;
    assert (Bytes.length msg = message_length_in_bytes) ;
    assert (Array.length signature = message_length_in_bytes * 8) ;
    let bits = Bit.le_bits_of_bytes_exn msg in
    let res = ref true in
    Bit.iteri
      (fun i b ->
        let pk1, pk2 = pk.(i) in
        let sig_i = signature.(i) in
        let exp_pk = sha256 sig_i in
        if Bit.is_one b then res := !res && Bytes.equal exp_pk pk2
        else res := !res && Bytes.equal exp_pk pk1)
      bits ;
    !res
end

include Lamport

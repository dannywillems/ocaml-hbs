let generate_256bits () = Bytes.init 32 (fun _ -> char_of_int @@ Random.int 256)

let sha256 bytes =
  Hex.to_bytes (`Hex Digestif.SHA256.(to_hex (digest_bytes bytes)))

let bytes_to_int b length =
  assert (length <= 7) ;
  let acc, _ =
    Bytes.fold_left
      (fun (acc, curr_exp) c ->
        let acc = acc + (curr_exp * int_of_char c) in
        let curr_exp = curr_exp * 8 in
        (acc, curr_exp))
      (0, 1)
      b
  in
  acc

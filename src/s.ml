module type T = sig
  val message_length_in_bytes : int

  type sk

  val sk_size_in_bytes : int

  type pk

  val pk_size_in_bytes : int

  type signature

  val signature_size_in_bytes : int

  val generate_keys : unit -> sk * pk

  val sign : Bytes.t -> sk -> signature

  val verify : Bytes.t -> pk -> signature -> bool
end

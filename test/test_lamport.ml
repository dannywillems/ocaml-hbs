open Utils

let test_sign_and_verify_random_message () =
  let msg =
    Bytes.init Hbs.Lamport.message_length_in_bytes (fun _ ->
        char_of_int (Random.int 256))
  in
  let sk, pk = Hbs.Lamport.generate_keys () in
  let signature = Hbs.Lamport.sign msg sk in
  assert (Hbs.Lamport.verify msg pk signature)

let test_sign_and_verify_wrong_message () =
  let msg =
    Bytes.init Hbs.Lamport.message_length_in_bytes (fun _ ->
        char_of_int (Random.int 256))
  in
  let msg' =
    Bytes.init Hbs.Lamport.message_length_in_bytes (fun _ ->
        char_of_int (Random.int 256))
  in
  let sk, pk = Hbs.Lamport.generate_keys () in
  let signature = Hbs.Lamport.sign msg sk in
  assert (not (Hbs.Lamport.verify msg' pk signature))

let test_sign_and_verify_wrong_vk_because_of_different_sk () =
  let msg =
    Bytes.init Hbs.Lamport.message_length_in_bytes (fun _ ->
        char_of_int (Random.int 256))
  in
  let sk, _ = Hbs.Lamport.generate_keys () in
  let _, pk' = Hbs.Lamport.generate_keys () in
  let signature = Hbs.Lamport.sign msg sk in
  assert (not (Hbs.Lamport.verify msg pk' signature))

let () =
  let open Alcotest in
  run
    ~verbose:true
    "Lamport"
    [ ( "Signature scheme properties",
        [ Alcotest.test_case
            "Sign and verify random message"
            `Quick
            (repeat 100 test_sign_and_verify_random_message);
          Alcotest.test_case
            "Sign and verify wrong message"
            `Quick
            (repeat 100 test_sign_and_verify_wrong_message);
          Alcotest.test_case
            "sign and verify with an invalid verifying key (different sk)"
            `Quick
            (repeat 100 test_sign_and_verify_wrong_vk_because_of_different_sk)
        ] ) ]

open Utils

let test_sign_and_verify_random_message () =
  let msg =
    Bytes.init Hbs.Wots.message_length_in_bytes (fun _ ->
        char_of_int (Random.int 256))
  in
  let sk, pk = Hbs.Wots.generate_keys () in
  let signature = Hbs.Wots.sign msg sk in
  assert (Hbs.Wots.verify msg pk signature)

let test_sign_and_verify_wrong_message () =
  let msg =
    Bytes.init Hbs.Wots.message_length_in_bytes (fun _ ->
        char_of_int (Random.int 256))
  in
  let msg' =
    Bytes.init Hbs.Wots.message_length_in_bytes (fun _ ->
        char_of_int (Random.int 256))
  in
  let sk, pk = Hbs.Wots.generate_keys () in
  let signature = Hbs.Wots.sign msg sk in
  assert (not (Hbs.Wots.verify msg' pk signature))

let test_sign_and_verify_wrong_vk_because_of_different_sk () =
  let msg =
    Bytes.init Hbs.Wots.message_length_in_bytes (fun _ ->
        char_of_int (Random.int 256))
  in
  let sk, _ = Hbs.Wots.generate_keys () in
  let _, pk' = Hbs.Wots.generate_keys () in
  let signature = Hbs.Wots.sign msg sk in
  assert (not (Hbs.Wots.verify msg pk' signature))

let () =
  let open Alcotest in
  run
    ~verbose:true
    "Wots"
    [ ( "Signature scheme properties",
        [ Alcotest.test_case
            "Sign and verify random message"
            `Quick
            (repeat 10 test_sign_and_verify_random_message);
          Alcotest.test_case
            "Sign and verify wrong message"
            `Quick
            (repeat 10 test_sign_and_verify_wrong_message);
          Alcotest.test_case
            "sign and verify with an invalid verifying key (different sk)"
            `Quick
            (repeat 10 test_sign_and_verify_wrong_vk_because_of_different_sk) ]
      ) ]
